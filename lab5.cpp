/*******************
  Caleb Browder
  ctbrowd
  Lab 5
  Lab Section: 4
  Anurata Hridi
*******************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  srand(unsigned (time(0)));

  //create the deck, assign each card a unique combination (suit/value)
  Card deck[52];
  for (int i = 0; i < 52; i++) {
    deck[i].suit = static_cast<Suit>(i % 4);
    deck[i].value = (i % 13) + 2;

    //this line was used to test that each card was unique
    //cout << i  << " " << deck[i].suit << " " << deck[i].value << endl;
  }

  //shuffle the deck, make hand from first 5 cards, sort the hand by suit
  random_shuffle(deck, deck + 52, myrandom);
  sort(deck, deck + 5, suit_order);

  //output the cards in hand (first 5 cards of deck)
  for (int i = 0; i <= 4; i++) {
    cout << setw(10) << get_card_name(deck[i]) << " of ";
    cout << get_suit_code(deck[i]) << endl;
  }

  return 0;
}


bool suit_order(const Card& lhs, const Card& rhs) {
  //main branch sorts by suit in order in enum at top of file
  if (lhs.suit < rhs.suit)
    return true;
  else if (lhs.suit > rhs.suit)
    return false;
  else {
    //else branch if suits are same, sorts by value
    if (lhs.value < rhs.value)
      return true;
    else
      return false;
  }
}

//returns the unicode character of the suit
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//outputs name of card, defualts to a number value
string get_card_name(Card& c) {
  switch(c.value){
    case 11: return "Jack";
    case 12: return "Queen";
    case 13: return "King";
    case 14: return "Ace";
    default: return to_string(c.value);
  }
}
